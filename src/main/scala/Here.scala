import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.layout.StackPane
import javafx.stage.Stage

object Main {
  def main(args: Array[String]): Unit =
    Application.launch(classOf[HelloWorld], args: _*)
}

class HelloWorld extends Application {
  override def start(primaryStage: Stage) {
    primaryStage.setTitle("HelloWorld!")
    val btn = new Button
    btn.setText("We have our ways of making you say hello World")
    btn.setOnAction((e: ActionEvent) => {
      println("Hello Jupiter")
    })

    val root = new StackPane
    root.getChildren.add(btn)
    primaryStage.setScene(new Scene(root, 600, 250))
    primaryStage.show
  }
}
